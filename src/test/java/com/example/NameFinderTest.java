/**
 * Created by Kostas Perifanos on 03/02/2015.
 */
package com.example;


import com.example.namefinder.boilerplate.HtmlCleaner;
import com.example.namefinder.nerengine.NerOpenNLP;
import com.example.namefinder.nerengine.NerStanford;
import com.example.namefinder.utils.NameFinderUtils;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;




public class NameFinderTest {

    public void checkResults(String [] names, String target){
        boolean bFound = false;
        for (String result : names) {
            //System.out.println(result);
            if (result.equals(target)) {
                bFound = true;
            }
        }
        assertTrue(bFound);
    }
   
   
    
    // test OpenNLP Engine
    @Test
    public void testSiteOpenNLP() throws IOException, BoilerpipeProcessingException, ClassNotFoundException {
        NerOpenNLP nameFinder = new NerOpenNLP();
        String url = "http://www.oracle.com/us/corporate/press/BoardofDirectors/index.html";
        String text = NameFinderUtils.getHtml(url);
        String[] results = nameFinder.apply(text);

        checkResults(results, "Jeffrey S . Berg");

    }


    // test OpenNLP Engine, with boilerplate removal
    @Test
    public void testSiteOpenNLPBoilerplate() throws IOException, BoilerpipeProcessingException, ClassNotFoundException {
        NerOpenNLP nameFinder = new NerOpenNLP();

        final HtmlCleaner boilerpipe = new HtmlCleaner();
        String url = "http://www.theguardian.com/politics/2015/feb/05/labour-donor-gulam-noon-miliband-balls-embarrassing-gaffes-anti-business";
        String text = boilerpipe.apply(url);

        String[] results = nameFinder.apply(text);

        checkResults(results, "Gulam Noon");
    }



    // test OpenNLP Engine, different url
    @Test
    public void testSiteOpenNLPBoilerplate2() throws IOException, BoilerpipeProcessingException, ClassNotFoundException {
        NerOpenNLP nameFinder = new NerOpenNLP();

        final HtmlCleaner boilerpipe = new HtmlCleaner();
        String url = " http://www.huffingtonpost.com/joseph-e-stiglitz/greek-eurozone-austerity_b_6607186.html"; //http://www.oracle.com/us/corporate/press/BoardofDirectors/index.html";
        String text = boilerpipe.apply(url);

        
        String[] names = nameFinder.apply(text);
        checkResults(names, "Herbert Hoover");

    }


    // test Stanford Engine, no boilerplate removal
    @Test
    public void testSiteStanford() throws IOException, BoilerpipeProcessingException, ClassNotFoundException {
        NerStanford nameFinder = new NerStanford();

        String url = " http://www.huffingtonpost.com/joseph-e-stiglitz/greek-eurozone-austerity_b_6607186.html"; //http://www.oracle.com/us/corporate/press/BoardofDirectors/index.html";
        String text = NameFinderUtils.getHtml(url);

        String[] names = nameFinder.apply(text);

        checkResults(names, "Alexis Tsipras");
    }
    
    // test Stanford Engine, with boilerplate removal
    @Test
    public void testSiteStanfordPBoilerplate() throws IOException, BoilerpipeProcessingException, ClassNotFoundException {
        NerStanford nameFinder = new NerStanford();
        final HtmlCleaner boilerpipe = new HtmlCleaner();
        String url = " http://www.huffingtonpost.com/joseph-e-stiglitz/greek-eurozone-austerity_b_6607186.html"; //http://www.oracle.com/us/corporate/press/BoardofDirectors/index.html";
        String text = boilerpipe.apply(url);
        String[] names = nameFinder.apply(text);
        checkResults(names, "Herbert Hoover");
    }

   


    /** Test if java.net.MalformedURLException is thrown
     * *
     */

    @Test
    public void testMalformedURL() {

        final HtmlCleaner boilerpipe = new HtmlCleaner();
        String url = "foo";
        try {
            String text = boilerpipe.apply(url);
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            assertTrue(e.getClass().getName().equals("java.net.MalformedURLException"));
        }
    }

}

