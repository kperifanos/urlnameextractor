/**
 * Created by kostasperifanos on 03/02/2015.
 */
package com.example.namefinder.nerengine;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.Span;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;


/** OpenNLP-based NER (https://opennlp.apache.org/documentation/manual/opennlp.html)
 *
 */

public class NerOpenNLP implements NerEngineInterface {

    private final Logger _log = LoggerFactory.getLogger(NerOpenNLP.class);


    /** apply named entity extraction
     *
     * @param text
     * @return String array of names extracted
     * @throws IOException
     */

    @Override
    public String [] apply(String text) throws IOException {

        _log.info("NerOpenNLP::apply");

        // Load the model file downloaded from OpenNLP
        // http://opennlp.sourceforge.net/models-1.5/en-ner-person.bin
        TokenNameFinderModel model;

        model = new TokenNameFinderModel(new File("res/en-ner-person.bin"));


        // Create a NameFinder using the model
        final NameFinderME finder = new NameFinderME(model);

        final Tokenizer tokenizer = SimpleTokenizer.INSTANCE;

        // Split the sentence into tokens
        final String[] tokens = tokenizer.tokenize(text);

        // Find the names in the tokens and return Span objects
        final Span[] nameSpans = finder.find(tokens);

        final String[] strings = Span.spansToStrings(nameSpans, tokens);

        return strings;
    }


}
