/**
 * Created by Kostas Perifanos on 03/02/2015.
 */
package com.example.namefinder.nerengine;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/** Stanford NLP-based NER (http://nlp.stanford.edu/software/CRF-NER.shtml)
 *
 */
public class NerStanford implements NerEngineInterface {
    private final Logger _log = LoggerFactory.getLogger(NerOpenNLP.class);


    //NER initializer
    private final String serializedClassifier = "res/english.all.3class.distsim.crf.ser.gz";


    /**
     * apply named entity extraction
     *
     * @param text
     * @return String array of names extracted
     * @throws IOException
     */
    @Override
    public String[] apply(String text) throws IOException, ClassNotFoundException {


        _log.info("NerStanford::apply");

        AbstractSequenceClassifier<CoreLabel> _classifier = CRFClassifier.getClassifier(serializedClassifier);
        List<String> names = new ArrayList<String>();

        //classify to character offsets, check if the token is PERSON and extract the token's value
        List<Triple<String, Integer, Integer>> triples = _classifier.classifyToCharacterOffsets(text);
        for (Triple<String, Integer, Integer> trip : triples) {
            if (trip.first().equals("PERSON")) {
                String person = text.substring(trip.second(), trip.third());
                names.add(person);
            }
        }
        return names.toArray(new String[names.size()]);
    }


}
