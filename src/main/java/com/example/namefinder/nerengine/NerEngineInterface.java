/**
 * Created by kostasperifanos on 03/02/2015.
 */
package com.example.namefinder.nerengine;

import java.io.IOException;


/** Simple interface to wrap NER engines
 *
 *  Can be further extended with initialization, cleanup, parametrization etc
 */
public interface NerEngineInterface {

    /** Apply NER to a given piece of text
     *
     * @param text
     * @return String array of names detected
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public String[] apply(String text) throws IOException, ClassNotFoundException;



}
