/**
 * Created by Kostas Perifanos on 04/02/2015.
 */
package com.example.namefinder.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/** Helpers
 *
 */
public final class NameFinderUtils {

    /** Grab html source given a url
     *
     * @param url url to grab
     * @return html code
     * @throws IOException
     */
    public static String getHtml(final String url) throws IOException {
        final StringBuilder sb = new StringBuilder();
        final URL oracle = new URL(url);
        final BufferedReader in = new BufferedReader(
                new InputStreamReader(oracle.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            sb.append(inputLine);
        }

        in.close();
        return sb.toString();
    }

}
