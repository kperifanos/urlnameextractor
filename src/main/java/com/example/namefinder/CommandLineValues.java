package com.example.namefinder;


import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

/** This class handles the programs arguments.
 *
 */
public class CommandLineValues {
    @Option(name = "-u", aliases = {"--url"}, required = true,
            usage = "url")
    private String url;


    @Option(name = "-n", aliases = {"--ner"}, required = false,
            usage = "Named Entity Recognition Engine, one of {STANFORD, OPENNLP}")
    private String _ner = "STANFORD";


    @Option(name = "-c", aliases = {"--clean"}, required = false,
            usage = "Apply boilerplate removal (clean up html code)")
    private boolean _clean = false;


    private boolean errorFree = false;


    /** Parse cmd line
     * *
     * @param args
     */
    public CommandLineValues(String... args) {
        CmdLineParser parser = new CmdLineParser(this);
        parser.setUsageWidth(80);
        try {
            parser.parseArgument(args);

            if ( !_ner.toUpperCase().equals("STANFORD")  &&
                !_ner.toUpperCase().equals("OPENNLP")){
                throw new CmdLineException("Unknown NER library:" + _ner);
            }

            System.out.println(String.format("Using %s Ner Engine", _ner));
            errorFree = true;
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
    }

    /**
     * Returns whether the parameters could be parsed without an
     * error.
     *
     * @return true if no error occurred.
     */
    public boolean isErrorFree() {
        return errorFree;
    }

    /**
     * Returns the url.
     *
     * @return The url to scan for names.
     */
    public String getUrl() {
        return url;
    }


    /**
     * Returns the ner engine to be used.
     *
     * @return The ner engine.
     */
    public String getNer() {
        return _ner.toUpperCase();
    }

    /** Cleanup html code / apply boilerplate
     * *
     * @return cleanup value
     */
    public boolean cleanup(){
        return _clean;
    }
}
