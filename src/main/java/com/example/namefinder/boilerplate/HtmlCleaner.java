/**
 * Created by Kostas Perifanos on 03/02/2015.
 */
package com.example.namefinder.boilerplate;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Simple boilerplate removal based on Boilerpile. See https://code.google.com/p/boilerpipe/
 */
public class HtmlCleaner implements BoilerplateRemoval {


    /** Cleanup html
     *
     * @param url to grab html
     * @return plain page text
     * @throws MalformedURLException
     * @throws BoilerpipeProcessingException
     */
    @Override
    public String apply(String url) throws MalformedURLException, BoilerpipeProcessingException {

        return ArticleExtractor.INSTANCE.getText(new URL(url));

    }

}
