/**
 * Created by Kostas Perifanos on 03/02/2015.
 */

package com.example.namefinder.boilerplate;

import de.l3s.boilerpipe.BoilerpipeProcessingException;

import java.net.MalformedURLException;



/** Simple inteface to wrap boilerplate removal tasks
 *
 */
public interface BoilerplateRemoval {

    public  String apply(String url) throws MalformedURLException, BoilerpipeProcessingException;

}
