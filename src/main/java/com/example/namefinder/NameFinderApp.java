/**
 * Created by Kostas Perifanos on 03/02/2015.
 */
package com.example.namefinder;


import com.example.namefinder.boilerplate.HtmlCleaner;
import com.example.namefinder.nerengine.NerEngineInterface;
import com.example.namefinder.nerengine.NerOpenNLP;
import com.example.namefinder.nerengine.NerStanford;
import com.example.namefinder.utils.NameFinderUtils;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.IOException;

import static java.lang.System.exit;
import static java.lang.System.out;


@SuppressWarnings("unused")
public class NameFinderApp {

    /** Get the NER Engine from cmd line
     * *
     * @param nerEngine
     * @return NerEngineInterface, the interface to NER object
     */
    private static NerEngineInterface getNerInterface(String nerEngine){
        
        if(nerEngine.equals("STANFORD")){
            return new NerStanford();
        }
        else return new NerOpenNLP();
        
    }

    /**
     * *
     * @param args
     * @throws IOException
     * @throws BoilerpipeProcessingException
     * @throws ClassNotFoundException
     */
    public static void  main( String[] args) throws IOException, BoilerpipeProcessingException, ClassNotFoundException {

        final CommandLineValues values = new CommandLineValues(args);
        final CmdLineParser parser = new CmdLineParser(values);

        try {
            parser.parseArgument(args);
            if(!values.isErrorFree()){
                exit(1);
            }
        } catch (CmdLineException e) {
            exit(1);
        }


       
        final String url = values.getUrl();
        final String nerType = values.getNer();
        final NerEngineInterface nameFinder = getNerInterface(nerType);
        
        
        out.println( String.format("Scanning URL %s", url) );
        
        String text;
        //clean up html?
        if(values.cleanup()) {
            HtmlCleaner cleaner = new HtmlCleaner();
            text= cleaner.apply(url);
            out.println( String.format("Text length is %s characters", text.length()));
        }
        else{
            text = NameFinderUtils.getHtml(url);
        }
        
        final String [] names = nameFinder.apply(text);
        out.println("==============================");
        for(String name: names){
            out.println( name);
        }
        out.println("==============================");
        return;
    }
}
