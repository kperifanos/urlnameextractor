Instructions
============

UrlNameExtractor implements a simple way of extracting named entities from any web page.

It is based on two different implementations of Named Entity Recognition, one from OpenNLP and one from Stanford NPL.
It also demonstrates the use of boilerplate removal which can also be useful in certain cases.



__Requires Java 8__


Run the tests
-------------

To run the tests: 

    mvn test


Run the app
-------------

This app accepts command line params that are controlling the NER engine to be used and if the html code will be cleaned before name extraction

    --url, -u   : The url to scan, should start with http://
    --ner, -n   : Optional, The NER engine to use, can be one of {STANFORD, OPENNLP}. STANFORD is the default engine [See http://nlp.stanford.edu/software/CRF-NER.shtml, https://opennlp.apache.org/documentation/manual/opennlp.html]
    --clean, -n : Optional, Remove boilerplate html code

To run this application with maven: 

    mvn compile exec:java -Dexec.args="--url http://www.theguardian.com/politics/2015/feb/05/labour-donor-gulam-noon-miliband-balls-embarrassing-gaffes-anti-business --ner stanford -c"

To build a standalone jar file and run it:
    
    mvn assembly:assembly
    java -jar target/UrlNameExtractor-1.0-SNAPSHOT-jar-with-dependencies.jar --url http://www.oracle.com/us/corporate/press/BoardofDirectors/index.html


Suggested improvements
----------------------
- This implementation demonstrates only the usage for the English language. By using models trained on specific languages, along with language detection we can improve NER accuracy.
- We can also train multiclass classifiers to detect Locations, Company Names etc
- We can use either machine learning and/or regular expressions to extract phone numbers or emails from raw, semistructured  or unstructured text
- Ways to improve precision/recall:
    - Better tokenization
    - Additional training on handheld data, maximize precision/recall/F1-measure (depending on the task) on human labeled data.
      During the training/ model evaluation phase, make sure that we are measuring performance on _unknown_ data. 
      Typically, this can be achieved by spliting the data sets in 3 parts: (training, validation, test). 
      Train data set is then used to train the model, validation is used for model selection/tuning and finally, the model is finally tested against the test set.
- Description extraction could potentially be modelled as a classification task at a sentence level. Possible features could be POS taggjng/syntax trees, presence of named entities (locations for example).
- Similar strategy could also be used for title classification, but this time taking into account html markup, sentence length, common title linguistic patterns etc













